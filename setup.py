from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['blueprint_gripper_control'],
    #scripts=['bin/sonar_roll'],
    #package_dir={'': 'lib'}
)

setup(**d)