A simple ROS node for controlling one or more joints on a [Blueprint Bravo underwater actuator.](https://blueprintlab.com/products/manipulators/reach-bravo/)

# Getting Started

Intall the [Blueprint lab protocol software](https://github.com/blueprint-lab/Blueprint_Lab_Software/tree/master/bplprotocol):

```
git clone https://github.com/blueprint-lab/Blueprint_Lab_Software.git
cd bplprotocol
pip install -e .
```

To use the ROS node in this repo, it needs to be built in a Catkin workspace:

```
mkdir -p blueprint_ws/src/ && cd blueprint_ws/src/
git clone https://gitlab.com/apl-ocean-engineering/blueprint_gripper_control
cd ..
catkin build
source devel/setup.bash
```

Then

```
...
```

# Test

To run the integrated unit tests, [pytest](https://docs.pytest.org/en/6.2.x/) and [pytest-pythonpath](https://github.com/ericpalakovichcarr/pytest-pythonpath) must be installed.  Then:

```
pytest
```


# Links

* [Blueprint Lab software on Github](https://github.com/blueprint-lab/Blueprint_Lab_Software)
* [Blueprint Lab software docs](https://blueprint-lab.github.io/Blueprint_Lab_Software/) as a Github pages site.

# License

This code is released under the [BSD License](LICENSE)